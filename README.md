# lpmv

### Limit pulseaudio max volume

Usage:
 - download `lpmv.sh`
 - add to your wm config:
    - on bspwm : `exec watch -n S /path/to/lpmv.sh NNN &`
    - on i3wm  : `exec --no-startup-id watch -n S /path/to/lpmv.sh NNN &`
 - where:
    - `S` = watch will run lpmv.sh each `S` seconds
    - `NNN` = max volume in % you want to limit pulseaudio - not mandatory argument (it defaults to 100%)
 - restart your wm.
