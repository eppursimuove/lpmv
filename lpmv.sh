#!/usr/bin/env bash
# -----------------------------------------------------
# Nome		: lpmv.sh
# Descrição	: limit pulseaudio max volume in $1 (default = 100%)
# Versão	: 0.2
# Autor		: Eppur Si Muove
# Contato	: eppur.si.muove@keemail.me
# Criação	: 31/12/2021
# Modificação	:
# Licença	: GNU/GPL v3.0
# CDNEditor	: vim
# -----------------------------------------------------

sink=$(pactl list sinks | \
	grep -B1 'RUNNING' | \
	cut -d'#' -f2 |
	head -1)

[ -z $sink ] && exit 0

vol=$(pactl list sinks | \
	grep -A8 'RUNNING' | \
	grep 'Volume:' | \
	cut -d/ -f2 | \
	tr -d ' ' | \
	tr -d '%')

[ $vol -gt ${1:-100} ] && pactl set-sink-volume $sink ${1:-100}%
